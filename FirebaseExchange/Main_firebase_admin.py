import firebase_admin
from firebase_admin import credentials
from firebase_admin import db
import requests
import json
from pprint import pprint


cred = credentials.Certificate("pricetracker-bcc25-firebase-adminsdk-jxgfg-47f58a7811.json")

firebase_admin.initialize_app(cred, {
'databaseURL': 'https://pricetracker-bcc25.firebaseio.com/'
                                    }
                              )
## Con este codigo añadimos un json
with open ('PriceTracker.json') as data_file:
    data=json.load(data_file)
    jsondata = json.dumps('PriceTracker.json')
pprint(data)

ref = db.reference('/')
ref.update(data)


## No modificar lo que viene a continuación. Dejar como copia de seguridad.
# Adding using Set -> Cargar a base de datos. Creo que borra todo lo anterior
'''
ref = db.reference('/')
ref.set({
    'Employee':
        {
            'emp1': {
                'name':'Manuel',
                'second_name':'Rubio',
                'age':26
            },
            'emp2': {
                'name': 'Javier',
                'second_name': 'Torró',
                'age':27
            }
        }
    }
)
'''

# Updating -> Añadimos sin eliminar lo anterior
'''
ref = db.reference('Employee')
emp_ref = ref.child('emp1')

emp_ref.update({
    'name':'Manu'
})
'''

# MultipleUpdating -> Cambiar varios valores en la base de datos
'''
ref = db.reference('Employee')
ref.update({
    'emp1/name':'Manu'
    'emp2/name':'Jatorna'
})
'''

# Adding using push
'''
ref = db.reference('Employee2')
emp_ref = ref.push({
    'name':'Manuel',
    'long_name':'Ángel',
    'email':'marutorna@gmail.com'
})
# Print key
print(emp_ref.key)
'''

# Obtain data using get
'''
ref = db.reference('Employee')
print(ref.get())
'''
